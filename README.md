# Django Channels

## Instalação

Para instalar o django channels basta rodar este comando:
```commandline
python -m pip install -U channels
```

Adicionar o channels no topo do INSTALLED_APPS em settings.py:
```python
INSTALLED_APPS = (]
    'channels',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    ...,
)
```

Adicionar a variável ASGI_APPLICATION com o nome de sua aplicação no settings.py:
```python
ASGI_APPLICATION = "myproject.asgi.application"
```

## Integração ao Django
Para haver o tratamento de acordo com cada protocolo pelo Channels no myproject/asgi.py esse código:
```python
import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')

application = ProtocolTypeRouter({
    "http": get_asgi_application(),
    "websocket": AuthMiddlewareStack(
        URLRouter(
            # Arquivo com as rotas de websocket(ex: app.routing.ws_urlpatterns
        )
    ),
})
```

Suba o redis no docker para servir como camada de canal(channels_layer) do channels, que servirá como o 
meio de comunicação entre processos diferentes, como este:
```commandline
docker run -p 6379:6379 -d redis:5
```

Rode o comando para instalar o módulo do channels para redis:

```commandline
python3 -m pip install channels_redis
```

E adicione a sua configuração no settings.py:
```python
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}
```

## Outros Pontos

### Acesso ao Banco de Dados
- Caso queira o acesso ao bd via django orm de forma assíncrona, coloque database_sync_to_async como wrapper do método que irá executar a chamada ao db.
Exemplo:
```python
from channels.db import database_sync_to_async

async def connect(self):
    self.username = await database_sync_to_async(self.get_name)()

def get_name(self):
    return User.objects.all()[0].name
```

### Rotas
- Com o channels, uma boa prática é usar um arquivo de rotas para websockets separado como o routing.py
- No arquivo routing.py em vez de passar ```url_patterns```, se passa ```ws_urlpatterns``` ou o nome que prefirir como nome deste array. Dentro deste array, passe as rotas websocket que serão tratadas por um consumer ao invés de uma view.

### Sync para Async
- Para utilizar processos síncronos de forma assíncrona, basta utilizar o wrapper ```async_to_sync```:
```python
from asgiref.sync import async_to_sync

def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )
```

### Autenticação
- Por causa do módulo AuthMiddlewareStack, pode-se fazer o gerenciamento de sessão através dos métodos 
```async_to_sync(login)(self.scope, self.scope["user"])``` or ```login(self.scope, self.scope["user"])```
  para se logar e ```async_to_sync(logout)(self.scope)``` or ```logout(self.scope)``` para deslogar e como 
  não se salva automáticamente usa-se ```self.scope["session"].save()``` para manter a sessão salva.
  
```python
from asgiref.sync import async_to_sync
from channels.auth import login

class SyncChatConsumer(WebsocketConsumer):

    ...

    def receive(self, text_data):
        ...
        self.user = 
        self.scope["session"].save()
```

### Segurança
- Para manter seguro as conexões websocket, o channels tem o métodos ```OriginValidator```, que é um wrapper do ```AuthMiddlewareStack``` ou ```SessionMiddlewareStack```, que recebe um destes e uma lista com os domínios e/ou o caminho completo com ou sem a porta:
```python
...
from channels.security.websocket import OriginValidator

application = ProtocolTypeRouter({

    "websocket": OriginValidator(
        AuthMiddlewareStack(
            URLRouter([
                ...
            ])
        ),
        [".goodsite.com", "http://.goodsite.com:80", "http://other.site.com"],
    ),
})
```  

Se já tiver domínios definidos na váriavel ```ALLOWED_HOSTS``` e que somente reutilizá-los basta usar o método ```AllowedHostsOriginValidator```:
```python
from channels.security.websocket import AllowedHostsOriginValidator

application = ProtocolTypeRouter({

    "websocket": AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter([
                ...
            ])
        ),
    ),
})
```
